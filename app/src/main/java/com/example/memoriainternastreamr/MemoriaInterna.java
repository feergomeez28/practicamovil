package com.example.memoriainternastreamr;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;



public class MemoriaInterna extends AppCompatActivity implements View.OnClickListener{

    TextView txtCedulaMI,txtApellidoMI,txtNombreMI,txtDatos;
    Button btnEscribirMI,btnLeerMI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memoria_interna);
        txtCedulaMI = (TextView)findViewById(R.id.txtCedulaMI);
        txtApellidoMI = (TextView)findViewById(R.id.txtApellidoMI);
        txtNombreMI = (TextView)findViewById(R.id.txtNombreMI);
        txtDatos = (TextView)findViewById(R.id.txtDatos);

        btnEscribirMI = (Button)findViewById(R.id.btnEscribirMI);
        btnLeerMI = (Button)findViewById(R.id.btnLeerMI);


        btnEscribirMI.setOnClickListener(this);
        btnLeerMI.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
    switch (v.getId()){
        case R.id.btnEscribirMI:
            try {
                OutputStreamWriter escritor = new OutputStreamWriter(openFileOutput("archivo.txt", Context.MODE_APPEND));
                escritor.write(txtCedulaMI.getText().toString()+","+txtApellidoMI.getText().toString()+","+txtNombreMI.getText().toString());
                escritor.close();
                txtNombreMI.setText("");
                txtApellidoMI.setText("");
                txtCedulaMI.setText("");
            }catch (Exception ex){
                Log.e("Archivo MI","Error en el archivo de escritura");
            }
            break;
        case R.id.btnLeerMI:
            try {
                BufferedReader lector = new BufferedReader(new InputStreamReader(openFileInput("archivo.txt")));
                String datos = lector.readLine();
                String[] listasPersonas = datos.split(";");
                for (int i = 0; i< listasPersonas.length; i++){
                    txtDatos.append(listasPersonas[i].split(",")[0] + " " +  listasPersonas[i].split(",")[1] + " " +
                            listasPersonas[i].split(",")[2]);
                }
                //txtDatos.setText("");
                lector.close();
            }catch (Exception ex){
                Log.e("Archivo MI","Error en la lectura del archivo"+ex.getMessage());
            }
            break;
        }
    }
}
